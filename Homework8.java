import java.util.Scanner;
public class Homework8 {
	
	public static void draw1(int n) {
		
		for(int i = 0 ; i<n ; i++) {
			System.out.print("*");
		}
		
	}
	
	public static void draw2(int n) {
		String s = "*";
		for(int i =1 ; i<n ; i++) {
			s = s+"*";
		}
		for(int i = 0 ; i<n ; i++) {
			System.out.println(s);
		}
		
	}
	
	public static void draw3(int n) {
		String s = "";
		for(int i =0 ; i<n ; i++) {
			s = s + String.valueOf(i+1);
		}
		for(int i = 0 ; i<n ; i++) {
			System.out.println(s);
		}
		
	}
	
	public static void draw4(int n) {
		String s = "";
		for(int i =n ; i>0 ; i--) {
			s = s + String.valueOf(i);
		}
		for(int i = 0 ; i<n ; i++) {
			System.out.println(s);
		}
		
	}
	
	public static void draw5(int n) {
		for(int j = 0 ; j<n ; j++) {
			for(int i = 0 ; i<n ; i++) {
				System.out.print(j+1);
			}
			System.out.println();
		}
	}
	
	public static void draw6(int n) {
		for(int j = n ; j>0 ; j--) {
			for(int i = 0 ; i<n ; i++) {
				System.out.print(j);
			}
			System.out.println();
		}
	}
	
	public static void draw7(int n) {
		int idex = 1;
		for(int i = 0 ; i<n ; i++) {
			for(int j =0 ; j<n ; j++) {
				System.out.print(idex++ + " ");
			}
			System.out.println();
		}
	}
	
	public static void draw8(double n) {
		double idex = Math.pow(n, 2);
		for(double i = n ; i>0 ; i--) {
			for(double j =n ; j>0 ; j--) {
				System.out.print((int)idex-- + " ");
			}
			System.out.println();
		}
	}
	
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		System.out.print("In put n : ");
		int Input = sc.nextInt();
		
		System.out.println("draw1 ");
		draw1(Input);
		
		System.out.println("\n\ndraw2 ");
		draw2(Input);
		
		System.out.println("\n\ndraw3 ");
		draw3(Input);
		
		System.out.println("\n\ndraw4 ");
		draw4(Input);
		
		System.out.println("\n\ndraw5 ");
		draw5(Input);
		
		System.out.println("\n\ndraw6 ");
		draw6(Input);
		
		System.out.println("\n\ndraw7 ");
		draw7(Input);
		
		System.out.println("\n\ndraw8 ");
		draw8((double)Input);
		
	 }

}
