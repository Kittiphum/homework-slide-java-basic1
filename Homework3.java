import java.util.Scanner;

public class Homework3 {
	
	public static void draw9(int n) {
		int num = -2 ;
		for(int i =0 ; i<n ; i++) {
			System.out.println(num = num +2);
		}
	}
	
	public static void draw10(int n) {
		int num = 0 ;
		for(int i =0 ; i<n ; i++) {
			System.out.println(num = num +2);
		}
	}
	
	public static void draw11(int n) {
		int idex = 1 ;
		int idexRow = 2;
		for(int i =0 ; i<n ; i++) {
			for(int j =0 ; j<n ; j++) {
				System.out.print(idex + " ");
				idex = idex + i+1;
			}
			idex = idexRow++;
			System.out.println();
		}
	}
	
	public static void draw12(int n) {
		String s[] = new String[n] ;
		for(int i =0 ; i< n ; i++) {
			s[i] = "*";
		}
		for(int i =0 ; i<n ; i++) {
			s[i] = "_";
			for(int j = 0 ; j<n ; j++) {
				System.out.print(s[j]);
			}
			s[i] = "*";
			System.out.println();
		}
	}
	
	public static void draw13(int n) {
		String s[] = new String[n] ;
		for(int i =0 ; i< n ; i++) {
			s[i] = "*";
		}
		for(int i =n ; i>0 ; i--) {
			s[i-1] = "_";
			for(int j = 0 ; j<n ; j++) {
				System.out.print(s[j]);
			}
			s[i-1] = "*";
			System.out.println();
		}
	}
	
	public static void draw14(int n) {
		String s[] = new String[n] ;
		for(int i =0 ; i< n ; i++) {
			s[i] = "_";
		}
		for(int i =0 ; i<n ; i++) {
			s[i] = "*";
			for(int j = 0 ; j<n ; j++) {
				System.out.print(s[j] + " ");
			}
			System.out.println();
		}
	}
	
	public static void draw15(int n) {
		String s[] = new String[n] ;
		for(int i =0 ; i< n ; i++) {
			s[i] = "*";
		}
		for(int i =n ; i>0 ; i--) {
			
			for(int j = 0 ; j<n ; j++) {
				System.out.print(s[j] + " ");
			}
			s[i-1] = "_";
			System.out.println();
		}
	}
	
	public static void draw16(int n) {
		String s[] = new String[n] ;
		for(int i =0 ; i< n ; i++) {
			s[i] = "_";
		}
		for(int i =0 ; i<n ; i++) {
			s[i] = "*";
			for(int j = 0 ; j<n ; j++) {
				System.out.print(s[j] + " ");
			}
			
			System.out.println();
		}
		for(int i =n-1 ; i>0 ; i--) {
			s[i] = "_";
			for(int j = 0 ; j<n ; j++) {
				System.out.print(s[j] + " ");
			}
			System.out.println();
		}
	}
	
	public static void draw17(int n) {
		String s[] = new String[n] ;
		for(int k =0 ; k< n ; k++) {
			s[k] = "-";
		}
//		
		for(int i =0 ; i<n ; i++) {
			for(int k =0 ; k<=i ; k++) {
				s[k] = String.valueOf(i+1);
			}
			for(int j =0  ; j <n ; j++) {
				System.out.print(s[j]);
			}
			
			System.out.println();
		}
		
		for(int i = n-1 ; i>0 ; i--) {
			for(int g =0 ; g<=i ; g++) {
				s[g] = String.valueOf(i);
			}
			for(int k = i ; k>=i ; k--) {
				s[k] = "-";
			}
			for(int j =0  ; j <n ; j++) {
				System.out.print(s[j]);
			}
			
			System.out.println();
		}
	}
	
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		System.out.print("In put n : ");
		int Input = sc.nextInt();
		
		System.out.println("\n\ndraw9");
		draw9(Input);
		
		System.out.println("\n\ndraw10");
		draw10(Input);
		
		System.out.println("\n\ndraw11");
		draw11(Input);
		
		System.out.println("\n\ndraw12");
		draw12(Input);
		
		System.out.println("\n\ndraw13");
		draw13(Input);
		
		System.out.println("\n\ndraw14");
		draw14(Input);
		
		System.out.println("\n\ndraw15");
		draw15(Input);
		
		System.out.println("\n\ndraw17");
		draw17(Input);
		
	 }
}
