public class Homework2 {
	
	public static String[][] multiplyTable(String [][] table) {
		int multiply = 0;
		for(int i =0 ; i<table.length ; i++) {
			
			for(int j =0 ; j<table.length ; j++) {
				
				multiply = Integer.parseInt(table[i][j]) *2 ;
				table[i][j] = String.valueOf(multiply);
				
			}
		}
		
		return table;
	}
	
	public static void displayTable(String [][] table) {
		
		for(int i =0 ; i<table.length ; i++) {
			for(int j =0 ; j<table.length ; j++) {
				System.out.print(table[i][j] + " ");
			}
			System.out.println();
		}
		
	}
	
	public static void main(String[] args) {
		
		String[][] table = {
				{ "1", "2", "3" }, 
			    { "4", "5", "6" }, 
			    { "7", "8", "9" }
		};
		
		System.out.println("original table : ");
		displayTable(table);
		table = multiplyTable(table);
		System.out.println("\n\ntable * 2 : ");
		displayTable(table);
	 }

}
